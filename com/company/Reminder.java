package by.stormnet.shimchik;

import java.time.Duration;
import java.time.LocalDateTime;

public class Reminder {

    public static void main(String[] args) {
        LocalDateTime nextNotificationTime = getNextNotificationTime();
        LocalDateTime currentDateTime = LocalDateTime.now().withSecond(0).withNano(0);

        System.out.println("Сейчас " + currentDateTime.toLocalTime().toString()
                + ", до ближайшего получасового интервала, который будет в " + nextNotificationTime.toLocalTime().toString()
                + ", осталось " + Duration.between(currentDateTime, nextNotificationTime).toMinutes() + " минут (-а/-ы).");
    }

    private static LocalDateTime getNextNotificationTime() {
        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime nextNotificationTime;

        if (currentDateTime.getMinute() < 30) {
            nextNotificationTime = currentDateTime.withMinute(30).withSecond(0).withNano(0);
        } else {
            nextNotificationTime = currentDateTime.withMinute(0).withSecond(0).withNano(0).plusHours(1);
        }

        return nextNotificationTime;
    }
}
